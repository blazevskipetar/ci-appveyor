import { CiAppveyorPage } from './app.po';

describe('ci-appveyor App', () => {
  let page: CiAppveyorPage;

  beforeEach(() => {
    page = new CiAppveyorPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
